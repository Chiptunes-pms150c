# Chiptunes on the Three Cent Microcontroller (Padauk PMS150c)

This plays ["Bitshift Variations in C Minor"][bsv] by Rob Miles on the PMS150c.
It needs no external components and is designed to run of a single CR927 lithium
coin cell. The MCU detects automatically when it is plugged into a line input,
and starts playing the music, and will go into a deep sleep once finished and
disconnected. The project uses [SDCC]'s assembler and the [Free PDK] toolchain.

Please check out the [full write-up] for more details!

## Pinout (SOT23-6)

            ___
    Audio =|o  |= N/C
      GND =|   |= VDD
    Debug =|___|= N/C

[bsv]: http://txti.es/bitshiftvariationsincminor
[SDCC]: http://sdcc.sourceforge.net/
[Free PDK]: https://free-pdk.github.io/
[full write-up]: https://gir.st/chiptunes.html
