# NOTE: tested against sdcc-4.1.0-rc1, dispdk@670e7bc, easypdkprog-1.3

.PHONY: all dis clean prog run sim
all: bsv.ihx

bsv.rel: bsv.asm
	sdaspdk13 -o $@ $^

bsv.ihx: bsv.rel
	sdldpdk -ni $@ $^

clean:
	rm -f bsv.ihx bsv.rel

dis: bsv.ihx
	dispdk 2A16 $^

prog: bsv.ihx
	easypdkprog -nPMS150C --noblankchk write $^

run:
	easypdkprog -nPMS150C -r3.0 start

sim: bsv.ihx
	  spdk -t PDK13 $^
